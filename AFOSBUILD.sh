# FIX CMD:
# find . -iname "*.go" -exec sed -i 's/github.com\/jaeles-project\/jaeles/gitlab.com\/snake-security\/jaeles/g' {} \;
#
# FIX go.mod too
#

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip dist/jaeles

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip jaeles... PASS!"
else
  # houston we have a problem
  exit 1
fi

rm -rf /opt/ANDRAX/bin/jaeles

cp -Rf dist/jaeles /opt/ANDRAX/bin/jaeles

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
